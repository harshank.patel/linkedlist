    function LinkedList() {
    var length = 0;
    var head = null
    var Node = function (element) {
        this.element = element
        this.next = null
    }

    this.size = function () {
        return length
    }

    this.head = function () {
        return head;
    }

    this.add = function (element) {
        var node = new Node(element)
        if (head === null) {
            head = node;
        } else {
            var current = head

            while (current.next != null) {
                current = current.next
            }
            current.next = node;
        }
        length++
    }

    this.remove = function (element) {
        if (length === 0) {
            return console.log("Linked list is empty")
        } else {
            var current = head

            if (current.element === element) {
                head = current.next
            } else {
                var prev = null
                while (current.element !== element && current.next !== null) {
                    prev = current
                    current = current.next
                }
                prev.next = current.next
            }
            length--;
        }
    }
    this.insertAt= function(element,position) {
        return insertDynamic(element,(length-position)+2)
    }

    const insertDynamic = function (element, position) {
    
        if (position > length+1 || position <= 0) {
            return console.log(`please insert valid position(0 to ${length})`)
        } else {
            var currentNode = head;
            var node = new Node(element)
            if (position === 1) {
                node.next = head;
                head = node;
                               
            } else if(position ===length+1) {
                while (currentNode.next != null) {
                    currentNode = currentNode.next
                }
                currentNode.next = node;

            }else {
                var i = 1;
                while (i < position-1) {
                    currentNode = currentNode.next;
                    i++
                }
                node.next = currentNode.next;
                currentNode.next = node;
            }
           length++;
        }
        
    }

    this.display = function () {
        var current = head

        if (head === null) {
            return console.log("linekd list is empty")
        }
        while (current.next) {
            process.stdout.write(`${current.element}-->`);
            current = current.next;
        }
        console.log(current.element)
    }

    this.removeAt = function(position) {
        return removeDynamic(position);
    }
    const removeDynamic = function(position){
        if (position > length  || position <= 0) {
            return console.log(`please insert valid position(0 to ${length})`)
        } else {
           if (position === 1) {
                head=head.next
            } else {
                var current = head
                var prev;
                let i = 0;
                while(i<position-1) {
                    prev=current
                    current = current.next
                    i++;
                }
                prev.next = current.next
            }
        } length--;
    }
}

var conga = new LinkedList();
conga.add("kitten");
conga.add("Puppy");
conga.add("Dog");
conga.add("sdf")
const len = conga.size()
conga.insertAt('harshank',2)
conga.insertAt('dfjaskdhw',1)
conga.insertAt('dfjaskdsdaaw', 6)

conga.display();
conga.removeAt(1)
conga.display()
